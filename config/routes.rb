Rails.application.routes.draw do
  resources :posts
  #devise_for :users
  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" } 
  #devise_for :users, :controllers => {:registrations => "registrations", omniauth_callbacks: "omniauth_callbacks"}

  root 'home#index'
  match '/help', to: 'home#help', via: 'get'
  match '/about', to: 'home#about', via: 'get'  
end
